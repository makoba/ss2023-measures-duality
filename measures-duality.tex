\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\usepackage{commands}
\bibliography{references.bib}

\title{Measures and Duality}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    These are notes for a talk I am giving in the PhD-seminar on \emph{Local Langlands for $\GL_2$} taking place in the summer term 2023 in Essen.
    The main reference for this talk is \cite{bushnell-henniart}.

    I thank the organizer of the seminar, Luca Marannino, for helping me with the preparation.
    Also, use these notes at your own risk, they probably contain mistakes (and feel free to tell me about these mistakes)!

    \section{Notation}

    We fix a non-archimedean local field $F$ with ring of integers $\oo$, maximal ideal $\pp \subseteq \oo$, uniformizer $\varpi \in \pp$, units $U_F = \oo^{\times}$ and residue field $\kk = \oo/\pp$ of characteristic $p$ and cardinality $q$.
    We consider the algebraic subgroups
    \[
        B \coloneqq \curlybr[\Bigg]{\begin{pmatrix}a & c \\ 0 & b\end{pmatrix}}, \,
        T \coloneqq \curlybr[\Bigg]{\begin{pmatrix}a & 0 \\ 0 & b\end{pmatrix}}, \,
        N \coloneqq \curlybr[\Bigg]{\begin{pmatrix}1 & c \\ 0 & 1\end{pmatrix}}, \,
        N' \coloneqq \curlybr[\Bigg]{\begin{pmatrix}1 & 0 \\ c' & 1\end{pmatrix}}, \,
        Z \coloneqq \curlybr[\Bigg]{\begin{pmatrix}a & 0 \\ 0 & a\end{pmatrix}} \subseteq \GL_2
    \]
    called the standard Borel, the standard torus, the unipotent radical of the standard Borel the unipotent radical of the opposite of the standard Borel (relative to the standard torus) and the center, respectively.
    We also consider the standard Iwahori subgroup
    \[
        I \coloneqq \GL_2(\oo) \times_{\GL_2(\kk)} B(\kk) = \set[\Bigg]{\begin{pmatrix}a & c \\ c' & b\end{pmatrix}}{a, b \in U_F, c \in \oo, c' \in \pp} \subseteq \GL_2(F).
    \]
    Finally, $G, G_1, G_2$ always denote locally profinite groups and $X, Y$ denote locally profinite sets.

    \section{Some group theory}

    The reference for this section is \cite[Sections 7.2, 7.3]{bushnell-henniart}.

    \begin{proposition}[Iwasawa decomposition] \label{prop:iwasawa}
        We have
        \[
            \GL_2(F) = B(F) \GL_2(\oo).
        \]
    \end{proposition}

    \begin{proof}
        The quotient $B \backslash \GL_2$ is a proper scheme (actually it is the projective line).
        The valuative criterion for properness thus implies that the map
        \[
            B(\oo) \backslash \GL_2(\oo) = (B \backslash \GL_2)(\oo) \to (B \backslash \GL_2)(F) = B(F) \backslash \GL_2(F)
        \]
        is a bijection.
    \end{proof}

    \begin{corollary} \label{cor:quot-compact}
        The quotient $B(F) \backslash \GL_2(F)$ is compact.
    \end{corollary}

    \begin{proof}
        By \Cref{prop:iwasawa} the map $\GL_2(\oo) \to B(F) \backslash \GL_2(F)$ is surjective.
        As $\GL_2(\oo)$ is compact this gives the desired result.
    \end{proof}

    \begin{proposition}[Cartan decomposition] \label{prop:cartan}
        We have
        \[
            \GL_2(F) = \bigsqcup_{a, b \in \ZZ, a \leq b} \GL_2(\oo) \begin{pmatrix}\varpi^a & 0 \\ 0 & \varpi^b\end{pmatrix} \GL_2(\oo).
        \]
    \end{proposition}

    \begin{proof}
        This follows immediately from the elementary divisor theorem.
    \end{proof}

    \begin{corollary}
        For any compact open subgroup $K \subseteq \GL_2(F)$ the quotient $G/K$ is countable.
    \end{corollary}

    \begin{proof}
        It suffices to show this for a single compact open subgroup, so we may assume that $K = \GL_2(\oo)$.
        Then $K \backslash \GL_2(F) / K$ is countable by \Cref{prop:cartan}.
        Moreover, every double coset $K g K$ is a finite union of left cosets $g_i K$ so that also $\GL_2(F) / K$ is countable.
    \end{proof}

    \begin{proposition}[Iwahori decomposition]
        The multiplication map
        \[
            (I \cap N'(F)) \times (I \cap T(F)) \times (I \cap N(F)) \to I
        \]
        is a bijective homeomorphism, and the same is true for any reordering of the three factors.
    \end{proposition}

    \begin{proof}
        This can be checked by an explicit calculation.
    \end{proof}

    \begin{remark}
        Note that we have $I \cap N'(F) = \ker(N(\oo) \to N(\kk))$, $I \cap T(F) = T(\oo)$ and $I \cap N(F) = N(\oo)$.
    \end{remark}

    \begin{remark}
        All of the above decompositions can be appropriately generalized from $\GL_2$ to the setting of reductive groups.
    \end{remark}

    \section{Haar measures}

    The reference for this section is \cite[Sections 3.1 - 3.4 and 7.4 - 7.6]{bushnell-henniart}.

    \begin{definition}
        \begin{itemize}
            \item
            We denote the space of compactly supported and locally constant $\CC$-valued functions on $X$ by $C_c^{\infty}(X)$.

            \item
            A \emph{measure on $X$} is a $\CC$-linear map
            \[
                I \colon C_c^{\infty}(X) \to \CC
            \]
            such that $I(f) \in \RR_{\geq 0}$ for all $f \in C_c^{\infty}(X)$ that are valued in $\RR_{\geq 0}$.

            \item
            Given an open subspace $X' \subseteq X$ we have $C_c^{\infty}(X') \subseteq C_c^{\infty}(X)$ via extension by $0$, and using this we can restrict measures on $X$ to measures on $X'$.
        \end{itemize}
        We will typically denote a measure by a symbol like $\mu$ and the associated map by
        \[
            f \mapsto I_{\mu}(f) = \int_X f(x) \, \dd \mu(x).
        \]
    \end{definition}

    \begin{remark}
        Given a measure $\mu$ on $X$ and a compact open subset $A \subseteq X$ we write $\mu(A) \coloneqq I_{\mu}(1_A)$.
        Then $\mu$ is uniquely determined by $\mu(A)$ for all $A$.
        In fact, giving a measure $\mu$ on $X$ is equivalent to giving a function
        \[
            \mu \colon \curlybr[\big]{\text{compact open subsets of $X$}} \to \RR_{\geq 0}
        \]
        such that $\mu(A \sqcup B) = \mu(A) + \mu(B)$ for all disjoint $A, B$.
    \end{remark}

    \begin{remark}
        The subset $\Meas(X) \subseteq C_c^{\infty}(X)^*$ of measures on $X$ is stable under addition and multiplication by scalars in $\RR_{\geq 0}$.
        In other words, it is a cone.
    \end{remark}

    \begin{remark}
        We have a natural isomorphism
        \[
            C_c^{\infty}(X) \otimes C_c^{\infty}(Y) \cong C_c^{\infty}(X \times Y), \qquad f_1 \otimes f_2 \mapsto \roundbr[\big]{(x, y) \mapsto f_1(x) f_2(y)}.
        \]
        Thus, given measures $\mu$ and $\nu$ on $X$ and $Y$ we can define a measure $\mu \otimes \nu$ on $X \times Y$ by declaring
        \[
            I_{\mu \otimes \nu}(f_1 \otimes f_2) \coloneqq I_{\mu}(f_1) I_{\nu}(f_2).
        \]
        For a general function $f \in C_c^{\infty}(X \times Y)$ we then have
        \[
            \int_{X \times Y} f(x, y) \, \dd (\mu \otimes \nu)(x, y) = \int_X \int_Y f(x, y) \, \dd \nu(y) \, \dd \mu(x) = \int_Y \int_X f(x, y) \, \dd \mu(x) \, \dd \nu(y).
        \]
    \end{remark}

    \begin{definition}
        \begin{itemize}
            \item
            We define two actions $\lambda_G$ and $\rho_G$ of $G$ on $C_c^{\infty}(G)$, that are respectively given by
            \[
                (\lambda_G(g) f)(x) = f(g^{-1} x) \quad \text{and} \quad (\rho_G(g) f)(x) = f(x g)
            \]
            for $g, x \in G$ and $f \in C_c^{\infty}(G)$.
            Note that both representations are smooth.

            \item
            A measure $\mu$ on $G$ is called a \emph{left (resp.\ right) Haar measure} if $\mu \neq 0$ and $I_{\mu}(\lambda_G(g) f) = I_{\mu}(f)$ (resp.\ $I_{\mu}(\rho_G(g) f) = I_{\mu}(f)$) for all $g \in G$ and $f \in C_c^{\infty}(G)$.
        \end{itemize}
    \end{definition}

    \begin{proposition}
        There exists a left Haar measure for $G$.
        It is unique up to a factor $c \in \RR_{> 0}$.

        Moreover, if $\mu$ is a left Haar measure on $G$ then the measure on $G$ given by
        \[
            f \mapsto \int_G f(x^{-1}) \, \dd \mu(x)
        \]
        is a right Haar measure for $G$.
    \end{proposition}

    \begin{lemma}
        Let $H \subseteq G$ be an open subgroup and let $\mu$ be a left Haar measure on $G$.
        Then the restriction of $\mu$ to $H$ is a left Haar measure on $H$.
    \end{lemma}

    \begin{definition}
        Let $\mu$ be a left Haar measure for $G$.
        For $g \in G$, the measure on $G$ given by
        \[
            f \mapsto \int_G f(x g) \, \dd \mu(x)
        \]
        is again a left Haar measure, so that there exists a unique constant $\delta_G(g) \in \RR_{> 0}$ such that
        \[
            \delta_G(g) \int_G f(x g) \, \dd \mu(x) = \int_G f(x) \, \dd \mu(x)
        \]
        for all $f \in C_c^{\infty}(G)$.
        Then we define the \emph{modular character of $G$ (or module of $G$)} as the map $\delta_G \colon G \to \RR_{> 0}$.
        It is in fact a continuous character (and even trivial on any compact open subgroup $K \subseteq G$).

        We call $G$ \emph{unimodular} if $\delta_G = 1$.
        This is equivalent to saying that a left Haar measure on $G$ is also a right Haar measure (and the other way around). If $G$ is unimodular, we also just say Haar measure without specifying left or right.
    \end{definition}

    \begin{lemma}
        Let $\mu_1$ be a left Haar measure on $G_1$ and let $\mu_2$ be a left Haar measure on $G_2$.
        Then $\mu_1 \otimes \mu_2$ is a left Haar measure on $G_1 \times G_2$.
    \end{lemma}

    \begin{lemma}
        Suppose we are given an action $\phi \colon G_1 \to \Aut(G_2)$ so that we can form the semidirect product $G_1 \ltimes G_2$.
        Suppose furthermore that we are given left Haar measures $\mu_1$ and $\mu_2$ on $G_1$ and $G_2$ respectively.
        Then $\mu_1 \otimes \mu_2$ is a left Haar measure on $G_1 \ltimes G_2$.

        If we moreover define $\delta_{\phi} \colon G_1 \to \RR_{> 0}$ by the formula
        \[
            \delta_{\phi}(g) \int_{G_2} f(\phi(g)(x)) \, \dd \mu_2(x) = \int_{G_2} f(x) \, \dd \mu_2(x)
        \]
        for $g \in G_1$ and $f \in C_c^{\infty}(G_2)$, then we have
        \[
            \delta_{G_1 \ltimes G_2}(g_1, g_2) = \delta_{G_1}(g_1) \cdot \delta_{G_2}(g_2) \cdot \delta_{\phi}(g_1^{-1})
        \]
        for $(g_1, g_2) \in G_1 \ltimes G_2$.
    \end{lemma}

    \begin{example}
        We now give a few important examples.

        \begin{itemize}
            \item
            We denote by $\mu_F$ the Haar measure on $F$ that is normalized by $\mu_F(\oo) = 1$.
            We then have $\mu_F(a + \pp^n) = q^{-n}$ for any $a \in F$ and $n \in \ZZ$.

            \item
            A Haar measure $\mu_{F^{\times}}$ on $F^{\times}$ is given by the formula
            \[
                \int_{F^{\times}} f(x) \, \dd \mu_{F^{\times}}(x) \coloneqq \int_F f(x) \abs{x}^{-1} \, \dd \mu_F(x).
            \]

            \item
            A (left and right) Haar measure $\mu_{\GL_2(F)}$ on $\GL_2(F)$ is given by the formula
            \[
                \int_{\GL_2(F)} f(x) \, \dd \mu_{\GL_2(F)} (x) \coloneqq \int_{M_2(F)} f(x) \abs{\det(x)}^{-2} \, \dd \mu_F^{\otimes 4}(x).
            \]
            In particular $\GL_2(F)$ is unimodular.

            \item
            As we have $N(F) \cong N'(F) \cong F$, $Z(F) \cong F^{\times}$ and $T(F) \cong F^{\times} \times F^{\times}$, we also know what their Haar measures are.

            \item
            The group $B(F)$ can be written as a semidirect product $B(F) = T(F) \ltimes N(F)$ so that we obtain a left Haar measure $\mu_{B(F)} = \mu_{T(F)} \otimes \mu_{N(F)}$ for it.

            However, $B(F)$ is not unimodular.
            Its modular character is given by
            \[
                \delta_{B(F)}(g) = \abs{a^{-1} b}, \qquad g = \begin{pmatrix}a & c \\ 0 & b\end{pmatrix}.
            \]

            \item
            The restriction of the Haar measure on $\GL_2(F)$ to $\GL_2(\oo)$ (and consequently also the one to $I$) coincides with $\dd \mu_F^{\otimes 4}$ as $\abs{\det(x)} = 1$ for $x \in \GL_2(\oo)$.
            The restricted Haar measure $\mu_I$ on $I$ can also be expressed as
            \[
                \int_I f(x) \, \dd \mu_I(x) = \int_{N'} \int_T \int_N f(n' t n) \, \dd \mu_N(n) \, \dd \mu_T(t) \, \dd \mu_{N'}(n')
            \]
            using the Iwasawa decomposition.
        \end{itemize}
    \end{example}

    \section{The contragredient representation}

    The reference for this section is \cite[Sections 2.8 - 2.10]{bushnell-henniart}.

    \begin{definition}
        We define a functor $(\blank)^{\vee} \colon \Rep(G)^{\op} \to \Rep(G)$ as follows:
        For a smooth $G$-representation $\pi$ we have the dual space $V_{\pi}^* = \Hom_{\CC}(V_{\pi}, \CC)$ that is equipped with a $G$-action
        \[
            \pi^* \colon G \to \Aut_{\CC}(V_{\pi}^*), \qquad g \mapsto \roundbr[\Big]{\varphi \mapsto \roundbr[\big]{\pi^*(g) \varphi \colon v \mapsto \varphi(\pi(g^{-1}) v)}}.
        \]
        However, the representation $\pi^*$ is not necessarily smooth.
        We thus define $\pi^{\vee} \coloneqq (\pi^*)^{\infty}$.
        We call $\pi^{\vee}$ the \emph{contragredient} or the \emph{smooth dual} of $\pi$.

        We denote the natural evaluation pairing $V_{\pi^{\vee}} \times V_{\pi} \to \CC$ by $\anglebr{\blank, \blank}$.
        It induces a natural morphism of abstract $G$-representations
        \[
            \delta_{\pi} \colon \pi \to \pi^{\vee *}, \qquad v \mapsto \roundbr[\big]{\varphi \mapsto \anglebr{\varphi, v} = \varphi(v)}
        \]
        that automatically has image inside $\pi^{\vee \vee} \subseteq \pi^{\vee *}$.
    \end{definition}

    \begin{proposition}
        We have the following properties (where $\pi$ always denotes a smooth $G$-representation):
        \begin{itemize}
            \item
            The natural morphism $V_{\pi^{\vee}}^K \to (V_{\pi}^K)^*$ is an isomorphism.

            \item
            The evaluation morphism $\delta_{\pi} \colon \pi \to \pi^{\vee \vee}$ is injective.

            \item
            The morphism $\delta_{\pi}$ is surjective (i.e.\ an isomorphism) if and only if $\pi$ is admissible.

            \item
            The functor $(\blank)^{\vee} \colon \Rep(G)^{\op} \to \Rep(G)$ is exact and faithful.

            \item
            If $\pi^{\vee}$ is irreducible, then so is $\pi$.
            The converse is true whenever $\pi$ is admissible.
        \end{itemize}
    \end{proposition}

    \section{The duality theorem}

    The reference for this section is \cite[Sections 3.4 - 3.5 and 7.7]{bushnell-henniart}.
    We fix a closed subgroup $H \subseteq G$.

    \begin{idea}
        We would like to understand how taking duals interacts with inducing representations from $H$ to $G$.
        Given a smooth representation $\sigma$ of $H$ one could expect to have a natural isomorphism
        \[
            \Ind_H^G (\sigma^{\vee}) \cong \roundbr[\big]{\cInd_H^G \sigma}^{\vee}
        \]
        that is induced by a ($G$-invariant) pairing of the form
        \[
            V_{\Ind_H^G (\sigma^{\vee})} \times V_{\cInd_H^G \sigma} \to \CC, \qquad (f, f') \mapsto \int_{H \backslash G} \anglebr[\big]{f(x), f'(x)} \, \dd \mu(x).
        \]
        Note that the function $\anglebr{f(x), f'(x)}$ is indeed well-defined and compactly supported on $H \backslash G$.

        The problem is now that we don't know what measure $\mu$ we should use to integrate.
        Maybe we would expect that there exists an essentially unique such measure that is invariant under right translation by $G$.
        But typically this is too much to ask for\dots
    \end{idea}

    \begin{definition}
        We set
        \[
            \delta_{H \backslash G} \coloneqq \delta_G|_H \cdot \delta_H^{-1} \colon H \to \RR_{> 0}.
        \]
        Then we define $C_c^{\infty}(H \backslash G)$ to be the space of locally constant functions $f \colon G \to \CC$ that are compactly supported modulo $H$ and such that
        \[
            f(h g) = \delta_{H \backslash G}(h) f(g)
        \]
        for all $h \in H$ and $g \in G$.
        Similarly to before we have an action $\rho_{H \backslash G}$ of $G$ on $C_c^{\infty}(H \backslash G)$ by right translation.
        $\rho_{H \backslash G}$ is a smooth representation.
    \end{definition}

    \begin{remark}
        The notation in the definition is confusing.
        The elements of $C_c^{\infty}(H \backslash G)$ are not functions on $H \backslash G$ but rather functions on $G$ that transform in a certain kind under left translation by $H$.
    \end{remark}

    \begin{proposition} \label{prop:quotient-measure}
        Let $\mu_H$ be a left Haar measure on $H$ and let $\mu_G$ be a right Haar measure on $G$.
        \begin{itemize}
            \item
            The map
            \[
                \rho_G \to \rho_{H \backslash G}, \qquad f \mapsto \roundbr[\Bigg]{\widetilde{f} \colon x \mapsto \int_H \delta_{G}(h)^{-1} f(hx) \, \dd \mu_H(h)}
            \]
            is a well-defined and surjective morphism of smooth $G$-representations.

            \item
            The morphism $I_{\mu_G} \colon C_c^{\infty}(G) \to \CC$ factors through $C_c^{\infty}(H \backslash G)$ to give
            \[
                I_{\mu_{H \backslash G}} \colon C_c^{\infty}(H \backslash G) \to \CC, \qquad f \mapsto \int_{H \backslash G} f(x) \, \dd \mu_{H \backslash G}(x).
            \]

            \item
            $I_{\mu_{H \backslash G}}$ is the unique (up to a scalar $c \in \RR_{> 0}$) non-trivial morphism $C_c^{\infty}(H \backslash G) \to \CC$ that satisfies
            \[
                I_{\mu_{H \backslash G}}(\rho_{H \backslash G}(g) f) = I_{\mu_{H \backslash G}}(f), \qquad f \in C_c^{\infty}(H \backslash G), g \in G
            \]
            and
            \[
                I_{\mu_{H \backslash G}}(f) \geq 0, \qquad f \in C_c^{\infty}(H \backslash G), f \geq 0.
            \]
        \end{itemize}
    \end{proposition}

    \begin{remark}
        This is again a warning about confusing notation.
        $\mu_{H \backslash G}$ is not a measure on $H \backslash G$, although we will think of it as such.
        And $\int_{H \backslash G} f(x) \, \dd \mu_{H \backslash G}(x)$ is not an integral on $H \backslash G$.
    \end{remark}

    \begin{theorem}[Duality Theorem] \label{thm:duality}
        Fix $\mu_{H \backslash G}$ as in \Cref{prop:quotient-measure} and let $\sigma$ be a smooth representation of $H$.
        We have a well-defined $G$-invariant pairing
        \[
            V_{\Ind_H^G(\delta_{H \backslash G} \otimes \sigma^{\vee})} \times V_{\cInd_H^G \sigma} \to \CC, \qquad (f, f') \mapsto \int_{H \backslash G} \anglebr[\big]{f(x), f'(x)} \, \dd \mu_{H \backslash G}(x)
        \]
        that is natural in $\sigma$.
        This pairing induces an isomorphism of smooth $G$-representations
        \[
            \Ind_H^G \roundbr[\big]{\delta_{H \backslash G} \otimes \sigma^{\vee}} \cong \roundbr[\big]{\cInd_H^G \sigma}^{\vee}.
        \]
    \end{theorem}

    \begin{example}
        We will be intersted in applying \Cref{thm:duality} in the situation where $G = \GL_2(F)$ and $H = B(F)$ and the representation $\sigma$ really is a representation of $T(F)$ (that we view as a representation of $B(F)$ that is trivial on $N(F)$).
        In that case the quotient $H \backslash G$ is compact by \Cref{cor:quot-compact} so that $\cInd_H^G \sigma \cong \Ind_H^G \sigma$ and $\delta_{H \backslash G} = \delta_H^{-1}$.
        Thus \Cref{thm:duality} then gives
        \[
            \Ind_{B(F)}^{\GL_2(F)} \roundbr[\big]{\delta_{B(F)}^{-1} \otimes \sigma^{\vee}} \cong \roundbr[\Big]{\Ind_{B(F)}^{\GL_2(F)} \sigma}^{\vee}.
        \]
    \end{example}

    \printbibliography
\end{document}