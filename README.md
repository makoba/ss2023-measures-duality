# ss2023-measures-duality

These are notes for a talk I am giving in the PhD-seminar (organized by Luca Marannino).
A compiled version can be found [here](https://makoba.gitlab.io/ss2023-measures-duality/measures-duality.pdf).